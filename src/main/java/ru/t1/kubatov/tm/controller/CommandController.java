package ru.t1.kubatov.tm.controller;

import ru.t1.kubatov.tm.api.controller.ICommandController;
import ru.t1.kubatov.tm.api.service.ICommandService;
import ru.t1.kubatov.tm.model.Command;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }


    @Override
    public void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        for (final Command command : commandService.getTerminalCommands()) {
            System.out.println(command.toString());
        }
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    @Override
    public void showDeveloperInfo() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Shamil Kubatov");
        System.out.println("E-MAIL: shamil.kubatov@mail.ru");
    }

    @Override
    public void showCommands() {
        System.out.println("[COMMANDS]");
        for (final Command command : commandService.getTerminalCommands()) {
            final String commandName = command.getName();
            if (commandName != null && !commandName.isEmpty()) System.out.println(commandName);
        }
    }

    @Override
    public void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
    }

    @Override
    public void showArguments() {
        System.out.println("[ARGUMENTS]");
        for (final Command command : commandService.getTerminalCommands()) {
            final String argument = command.getArgument();
            if (argument != null && !argument.isEmpty()) System.out.println(argument);
        }
    }

    @Override
    public void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported");
        System.exit(1);
    }

}
