package ru.t1.kubatov.tm.component;

import ru.t1.kubatov.tm.api.component.IBootstrap;
import ru.t1.kubatov.tm.api.controller.ICommandController;
import ru.t1.kubatov.tm.api.controller.IProjectController;
import ru.t1.kubatov.tm.api.controller.ITaskController;
import ru.t1.kubatov.tm.api.repository.ICommandRepository;
import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.api.service.ICommandService;
import ru.t1.kubatov.tm.api.service.IProjectService;
import ru.t1.kubatov.tm.api.service.ITaskService;
import ru.t1.kubatov.tm.constant.ArgumentConstant;
import ru.t1.kubatov.tm.constant.CommandConstant;
import ru.t1.kubatov.tm.controller.CommandController;
import ru.t1.kubatov.tm.controller.ProjectController;
import ru.t1.kubatov.tm.controller.TaskController;
import ru.t1.kubatov.tm.repository.CommandRepository;
import ru.t1.kubatov.tm.repository.ProjectRepository;
import ru.t1.kubatov.tm.repository.TaskRepository;
import ru.t1.kubatov.tm.service.CommandService;
import ru.t1.kubatov.tm.service.ProjectService;
import ru.t1.kubatov.tm.service.TaskService;
import ru.t1.kubatov.tm.util.TerminalUtil;

import java.util.Scanner;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskController taskController = new TaskController(taskService);

    @Override
    public void run(final String[] args) {
        parseArguments(args);
        parseCommands();
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private void parseCommands() {
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConstant.VERSION:
                commandController.showVersion();
                break;
            case CommandConstant.HELP:
                commandController.showHelp();
                break;
            case CommandConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            case CommandConstant.COMMANDS:
                commandController.showCommands();
                break;
            case CommandConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case CommandConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArgumentError();
        }
        exit();
    }

    private void exit() {
        System.exit(0);
    }

}
