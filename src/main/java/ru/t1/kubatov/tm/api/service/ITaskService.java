package ru.t1.kubatov.tm.api.service;

import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

}