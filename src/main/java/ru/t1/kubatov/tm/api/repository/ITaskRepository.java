package ru.t1.kubatov.tm.api.repository;

import ru.t1.kubatov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void deleteAll();

}
