package ru.t1.kubatov.tm.api.repository;

import ru.t1.kubatov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void deleteAll();

}
