package ru.t1.kubatov.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();

}
