package ru.t1.kubatov.tm.service;

import ru.t1.kubatov.tm.api.repository.IProjectRepository;
import ru.t1.kubatov.tm.api.service.IProjectService;
import ru.t1.kubatov.tm.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final Project project) {
        if(project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public void deleteAll() {
        projectRepository.deleteAll();
    }

    @Override
    public Project create(String name) {
        if(name == null || name.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        return projectRepository.add(project);
    }

    @Override
    public Project create(String name, String description) {
        if(name == null || name.isEmpty()) return null;
        if(description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return projectRepository.add(project );
    }

}
